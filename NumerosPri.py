#.........Realice un código que determine si un  número  es primo o no es primo.

#Autor....José Guamán

#Correo...jfguamancumbicos@gmail.com

def numero_primo ():
    while True:
        try:
            num = input("Ingrese un Numero y presione enter para finalizar el codigo ")
            cont = 0
            if num.lower() in "fin":
                print()
                print("Terminado")
                break
            num = int(num)
            for A in range(1, num + 1):
                if num % A == 0:
                    cont += 1
            if cont == 2:
                    print("El numero", [num], "es Primo.")
            else:
                    print("El numero ", [num], " No es Primo.")

        except ValueError:
                    print("Ingrese un numero Valido!")

numero_primo()
